---
fontsize: 12pt
lang: fr
lieu: UNIVERSITÉ DE SHERBROOKE
title: RAPPORT DE STAGE
presented-to: Sylvie Lamarche, conseillère en développement professionnel
author: |
        | Matthieu Daoust, stagiaire
        | Olympus
        | DevOps
        | T1
date: 10 août 2020
geometry: margin=1in
header-includes:
    - \usepackage{times}
    - \usepackage{setspace}
    - \setstretch{1.5}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \renewcommand{\headrulewidth}{0pt}
    - \fancyhf{}
    - \lfoot{Matthieu Daoust}
    - \cfoot{\thepage}
    - \rfoot{Matthieu.Daoust@USherbrooke.ca}
---

# Partie A: Stage en développement DevOps

## Mise en contexte

J'ai effectué mon stage dans le département de recherche et développement d'Olympus NDT Canada qui est situé à dans la ville de Québec, au Canada. Le département a récemment commencé une transition vers des meilleurs pratiques de développement logiciel en se basant, entre autres, sur la philosophie DevOps. Dans sa transformation, le département a créé une équipe spécialement dédiée à favoriser l'utilisation des meilleurs pratiques dans les équipes de développeurs. Une des missions de l'équipe DevOps est de migrer les ressources physiques vers des ressources virtuelles dans un écosystème infonuagique. C'est dans cette équipe que j'ai eu la chance de travailler pendant trois mois.

## Résumé du mandat

Lors de mon stage, plusieurs tâches m'ont été confié afin d'aider l'équipe DevOps. Parmi celles-ci, j'ai intégré des outils afin d'aider les développeurs, j'ai effectué la configuration de l'analyse de code dans le _pipeline_ d'intégration continue et j'ai créé des _cookbooks Chef_ pour gérer et déployer des configurations des machines d'intégration continue sous Linux. On m'a aussi confié le mandat de configurer des alertes de dépassement de budget dans la plateforme infonuagique afin d'éviter une perte de contrôle des coûts.

## Conclusion sur mon expérience de stage

Après avoir travaillé chez Olympus NDT Canada, je suis heureux d'avoir vécu une expérience positive d'apprentissage et de collaboration au sein de l'équipe DevOps. Cela a confirmé mon intérêt à aider les développeurs à utiliser de meilleures pratiques.

# Partie B

## L'environnement

> _Décrivez votre milieu de travail, indiquez vos zones de confort et d’inconfort quant à ces conditions et expliquez pourquoi._

Les employés de l'entreprise dans laquelle j'ai fait mon stage ont l'habitude de travailler dans un espace physique commun. Certains événements ont fait en sorte qu'ils ont été forcés à être complètement distribué à plusieurs endroits géographiques. J'ai donc eu la chance de joindre une équipe nouvellement à distance et d'observer certaines difficultés vécues durant cette période de transition. Même si j'ai adoré mon expérience à distance, j'aimerais être en mesure de vivre au moins une expérience de stage en présentiel afin de mieux comparer mes expériences à distances.

Au début de mon stage, on m'a offert des conseils ergonomiques pour organiser mon environnement de travail. L'application de ces conseils dans mon bureau m'as permis d'éviter des inconforts et des blessures reliés au travail avec un ordinateur portable.

L'équipe dans laquelle j'étais directement impliqué utilisait des concepts de la méthodologie Scrum, dont les _Daily Scrum Meeting_. Ces réunions journalières permettent d'informer quotidiennement l'ensemble des membres de l'équipe de l'avancement global des projets, ainsi que d'aider ceux qui ont des bloquant à poursuivre leur travail. L'utilisation de cette cérémonie fait en sorte que la charge de travail est distribuée dans l'équipe. Aussi, chaque semaine, des rencontres individuelles étaient prévus entre le superviseur et chaque membre de l'équipe. Ces rencontres favorisent une communication ouverte et permettent de partager de la rétroaction.

## Les connaissances

> _Que savez-vous maintenant, qui vous aidera pour vos prochains stages ou votre emploi? Pourquoi?_

Durant ce stage, j'ai acquis de nouvelles connaissances qui me seront utiles tout au long de ma vie.

D'une part, j'ai approfondi mes connaissances du système d'exploitation Linux. Bien que j'utilise ce système d'exploitation quotidiennement, j'ai pu l'utiliser dans des contextes auxquels j'étais moins à l'aise, tel que des environnements d'intégrations en continue. J'ai aussi eu l'occasion d'utiliser plusieurs langages de script tel que Python, Bash et Ruby. Une connaissance solide du système d'exploitation Linux ainsi que la capacité d'utiliser un ou des langages de script sont des compétences importantes à acquérir afin de devenir un professionnel DevOps.

D'autre part, j'ai appris à utiliser de nouveaux outils d'automatisations, tel que Chef et la composante d'intégration en continue (CI) de GitLab. À travers l'automatisation de processus, j'ai pu approfondir mes connaissances des outils de base tel que Git et Python. L'acquisition d'expérience en automatisation est cruciale afin d'être en mesure de déployer des infrastructures avec du code, au lieu d'utiliser des processus manuel et sujet aux erreurs.

## Le professionnalisme

> _Avez-vous fait des apprentissages particuliers en lien avec certaines règles ou normes et quel comportement avez-vous adopté?_

J'ai l'habitude d'utiliser des outils de communications et de collaboration qui permettent de personnaliser les moments où l'utilisateur est notifié lors de nouveaux messages. Cela permet de créer facilement des frontières temporelles entre le travail et la vie personnelle. Avec ces outils de communications, l'utilisateur est responsable de faire respecter les limites de son choix. Il existe cependant des outils qui ne permettent pas de créer ces limites facilement. C'est le cas notamment de certains clients de courriels électroniques. Pour contourner ces limitations techniques, il est parfois nécessaire de modifier la culture de l'entreprise. C'est ce qu'Olympus a fait récemment. En effet, lors de mon stage, une nouvelle politique est apparue chez Olympus. Elle stipule que les courriels ne doivent pas être envoyés les soirs et les fins de semaines.

Avant l'apparition de cette politique, il m'arrivait à l'occasion d'envoyer des courriels à l'extérieur des heures normales de travail. Pour que mes courriels s'envoient le lendemain matin, j'ai pris l'habitude de programmer l'heure d'envoi de mes courriels. Parfois, il m'est arrivé de cliquer manuellement sur le bouton d'envoi lorsque c'était plus pratique.

## La connaissance de soi

> _Parlez-nous de vos valeurs et de leur rôle dans cette expérience._

Les valeurs d'Olympus sont l'intégrité, l'empathie, la vision à long terme, l'agilité et l'unité. Durant ce stage, je me suis rendu compte que mes valeurs sont similaires à celle de mon employeur. En effet, ces valeurs ont eu une influence sur mon expérience de stage.

D'abord, j'ai apprécié la confiance qui règne dans l'équipe. Cela fait en sorte que tous les membres agissent constamment de bonne foi et avec intégrité. Aussi, en travaillant uni dans la même direction, nous sommes plus fort et nous pouvons accomplir de plus grandes choses.

Ensuite, je me suis souvent soucié de la façon dont mon travail allait être utilisé par d'autres personnes et cette empathie a eu pour effet que j'ai collaboré avec des membres de d'autres équipes. J'ai pu confirmer que mon travail avait un impact positif et c'était valorisant. Aussi, je suis heureux de constater qu'en plus de répondre à un besoin à court terme, mon travail aura aussi une valeur à long terme.

Enfin, j'ai su rapidement modifier la planification de mon travail lorsque c'était nécessaire. Cette agilité m'as permis de m'adapter aux nombreux changements qui sont survenu au cours de mon stage.
